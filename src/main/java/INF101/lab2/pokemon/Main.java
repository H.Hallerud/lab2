package INF101.lab2.pokemon;

public class Main {
    public static Pokemon pokemon1 = new Pokemon("Pikachu", 94, 12);
    public static Pokemon pokemon2 = new Pokemon("Oddish", 100, 3);
    public static void main(String[] args) {
        ///// Oppgave 6
        // Have two pokemon fight until one is defeated

        while (true){
            if (pokemon1.isAlive() == false){
                System.out.println("test22");
                break;
            }
            pokemon1.attack(pokemon2);
            
            if (pokemon2.isAlive() == false){
                System.out.println("test");
                break;    
            }
            pokemon2.attack(pokemon1);
        }
    }
}
